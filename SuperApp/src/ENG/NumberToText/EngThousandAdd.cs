﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class EngThousandAdd : IConvertNumber
    {
        public string NumberToString(int num)
        {
            EngDicrionaryContainer dictionary = new EngDicrionaryContainer();
            string returnText = "";
            int tmp;
            string numberString = num.ToString();
            int numLenght = numberString.Length;
            bool teenChecker = true;
            for (int i = num.ToString().Length; i > 0; i--)
            {
                tmp = Convert.ToInt32(num.ToString()[num.ToString().Length - i].ToString());
                switch (i)
                {
                    case 1 when (teenChecker):
                        returnText += new EngFirstAdd().NumberToString(tmp) + " " + dictionary.thousand;
                        break;
                    case 2:
                        if (Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) < 20 && Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) >= 10)
                        {
                            teenChecker = false;
                            returnText += new EngTeensAdd().NumberToString(Convert.ToInt32(numberString[numLenght - 1].ToString())) + " " + dictionary.thousand;
                        }
                        else
                        {
                            returnText += new EngTensAdd().NumberToString(tmp);
                        }
                        break;
                    case 3:
                        returnText += new EngHundredAdd().NumberToString(tmp);
                        break;
                    case 4:
                        returnText += new EngFirstAdd().NumberToString(tmp);
                        break;
                }

            }
            return returnText + " ";
        }
    }
}
