﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class EngHundredAdd : IConvertNumber
    {
        public string NumberToString(int num)
        {
            EngDicrionaryContainer dictionary = new EngDicrionaryContainer();
            if (num != 0)
            {
                return new EngFirstAdd().NumberToString(num) + " " + dictionary.hundred + " ";
            }
            return "";
        }
    }
}
