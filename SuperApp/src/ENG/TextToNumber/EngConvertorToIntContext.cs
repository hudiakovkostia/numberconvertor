﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class EngConvertorToIntContext
    {
        private IStringToNumber _strategy;
        public EngConvertorToIntContext()
        { 
        }
        public EngConvertorToIntContext(IStringToNumber strategy)
        {
            this._strategy = strategy;
        }
        public void SetConvertToIntStrategy(IStringToNumber strategy)
        {
            this._strategy = strategy;
        }
        public int DoConvertToInt(string textNum)
        {
            if (_strategy == null)
                return 0;
            return this._strategy.StringToNumber(textNum);
        }
    }
}
