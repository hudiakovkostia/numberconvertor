﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class EngConvertTensToInt : IStringToNumber
    {
        public int StringToNumber(string textNumber)
        {
            EngDicrionaryContainer dictionary = new EngDicrionaryContainer();
            foreach (var i in dictionary.tens)
            {
                if (i.Value == textNumber)
                {
                    return Convert.ToInt32(i.Key.ToString() + "0");
                }
            }
            return 0;
        }
    }
}
