﻿using System;

namespace SuperApp
{
    class EngNumberConvertor:ILanguageChoose
    {
        public string ConvertNumberToText(int number)
        {          
            bool isTeen = true;
            string numberString = number.ToString();
            int numLenght = number.ToString().Length;
            string finalText = "";
            for (int i = numLenght; i > 0; i--)
            {
                int numberToConvert = Convert.ToInt32(numberString[numLenght - i].ToString());
                EngConvertorContext context = new EngConvertorContext();
                switch (i)
                {
                    case 1 when (isTeen):
                        if (number != 0)
                        {
                            context.SetConvertStrategy(new EngFirstAdd());
                        }
                        else
                        {
                            return "zero";
                        }
                        break;
                    case 2:
                        if (Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) < 20 &&
                            Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) >= 10)
                        {
                            isTeen = false;
                            context.SetConvertStrategy(new EngTeensAdd());
                            numberToConvert = Convert.ToInt32(numberString[numLenght - 1].ToString());
                        }
                        else
                        {
                            context.SetConvertStrategy(new EngTensAdd());
                        }
                        break;
                    case 3:
                        context.SetConvertStrategy(new EngHundredAdd());
                        break;
                    case 4:
                        string thousandNum = numberString[i - 4].ToString();
                        if (numLenght >= 5)
                        {
                            thousandNum += numberString[i - 3];
                            if (numLenght >= 6)
                            {
                                thousandNum += numberString[i - 2];
                                if (numLenght >= 7)
                                {
                                    thousandNum += numberString[i - 1];
                                }
                            }
                        }
                        numberToConvert = Convert.ToInt32(thousandNum);
                        context.SetConvertStrategy(new EngThousandAdd());
                        break;
                }
                finalText += context.DoConvertToText(numberToConvert);
            }
            return finalText;
        }
        public int ConvertTextToNumber(string number)
        {
            string[] numberList = number.Split(' ');
            int finalNumber = 0;
            int tmpFirst = 0;
            bool isFirstMultiplication = false;
            bool isThousandMultiplication = false;
            bool isHundredMultiplication = false;
            for (int i = 0; i < numberList.Length; i++)
            {
                bool isMultiplication = false;
                EngConvertorToIntContext doConvert = new EngConvertorToIntContext();
                EngStringChecker isSomeNum = new EngStringChecker();
                if (isSomeNum.FirstChecker(numberList[i]))
                {
                    if (isThousandMultiplication)
                    {
                        isFirstMultiplication = true;
                        isMultiplication = true;
                    }
                    doConvert.SetConvertToIntStrategy(new EngConvertFitstsToInt());
                }
                if (isSomeNum.TeenChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new EngConvertTeensToInt());
                }
                if (isSomeNum.TenChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new EngConvertTensToInt());
                }
                if (isSomeNum.HundredChecker(numberList[i]))
                {
                    isMultiplication = true;
                    isHundredMultiplication = true;
                    doConvert.SetConvertToIntStrategy(new EngConvertHundredToInt());
                }
                if (isSomeNum.ThousendChecker(numberList[i]))
                {
                    isMultiplication = true;
                    isThousandMultiplication = true;
                    doConvert.SetConvertToIntStrategy(new EngConvertThousendToInt());
                }
                if (isMultiplication)
                {
                    if (isFirstMultiplication && isThousandMultiplication)
                    {
                        tmpFirst += doConvert.DoConvertToInt(numberList[i]);
                        isFirstMultiplication = false;
                    }
                    else
                    {
                        if (isThousandMultiplication && isHundredMultiplication)
                        {
                            finalNumber += tmpFirst * doConvert.DoConvertToInt(numberList[i]);
                            isThousandMultiplication = false;
                            isHundredMultiplication = false;
                        }
                        else
                        {
                            finalNumber *= doConvert.DoConvertToInt(numberList[i]);
                            isHundredMultiplication = false;
                        }
                    }
                }
                else
                {
                    finalNumber += doConvert.DoConvertToInt(numberList[i]);
                }
            }
            return finalNumber;
        }
    }
}

