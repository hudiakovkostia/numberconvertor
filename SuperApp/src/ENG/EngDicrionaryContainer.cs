﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class EngDicrionaryContainer
    {
        public Dictionary<int, string> firsts = new Dictionary<int, string>()
        {
            {1, "one"},
            {2, "two"},
            {3, "three"},
            {4, "four"},
            {5, "five"},
            {6, "six"},
            {7, "seven"},
            {8, "eight"},
            {9, "nine"}
        };
        public Dictionary<int, string> teens = new Dictionary<int, string>()
        {
            {0, "ten"},
            {1, "eleven"},
            {2, "twelve"},
            {3, "thirteen"},
            {4, "fourteen"},
            {5, "fifteen"},
            {6, "sixteen"},
            {7, "seventeen"},
            {8, "eighteen"},
            {9, "nineteen" }
        };
        public Dictionary<int, string> tens = new Dictionary<int, string>()
        {
            {2, "twenty"},
            {3, "thirty"},
            {4, "forty"},
            {5, "fifty"},
            {6, "sixty"},
            {7, "seventy"},
            {8, "eighty"},
            {9, "ninety" }
        };
        public string hundred = "hundred";
        public string thousand = "thousand";
    }
}
