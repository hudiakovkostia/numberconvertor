﻿using System;

namespace SuperApp
{
    class RuNumberConvertor:ILanguageChoose
    {
        public string ConvertNumberToText(int number)
        {          
            bool isTeen = true;
            string numberString = number.ToString();
            int numLenght = number.ToString().Length;
            string finalText = "";
            for (int i = numLenght; i > 0; i--)
            {
                int numberToConvert = Convert.ToInt32(numberString[numLenght - i].ToString());
                var context = new RuConvertorContext();
                switch (i)
                {
                    case 1 when (isTeen):
                        if (number != 0)
                        {
                            context.SetConvertStrategy(new RuFirstsAdd());
                        }
                        else
                        {
                            return "ноль";
                        }
                        break;
                    case 2:
                        if (Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) < 20 &&
                            Convert.ToInt32(numberString[numLenght - 2].ToString() + numberString[numLenght - 1].ToString()) >= 10)
                        {
                            isTeen = false;
                            context.SetConvertStrategy(new RuTeensAdd());
                            numberToConvert = Convert.ToInt32(numberString[numLenght - 1].ToString());
                        }
                        else
                        {
                            context.SetConvertStrategy(new RuTensAdd());
                        }
                        break;
                    case 3:
                        context.SetConvertStrategy(new RuHundredsAdd());
                        break;
                    case 4:
                        string thousandNum = numberString[i - 4].ToString();
                        if (numLenght >= 5)
                        {
                            thousandNum += numberString[i - 3];
                            if (numLenght >= 6)
                            {
                                thousandNum += numberString[i - 2];
                            }
                        }
                        numberToConvert = Convert.ToInt32(thousandNum);
                        context.SetConvertStrategy(new ThousandAdd());
                        break;
                }
                finalText += context.DoConvertToText(numberToConvert);
            }
            return finalText;
        }
        public int ConvertTextToNumber(string number)
        {
            string[] numberList = number.Split(' ');
            int finalNumber = 0;
            for (int i = 0; i < numberList.Length; i++)
            {
                bool isMultiplication = false;
                RuConvertorToIntContext doConvert = new RuConvertorToIntContext();
                RuStringChecker isSomeNum = new RuStringChecker();
                if (isSomeNum.FirstChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new RuConvertFitstsToInt());
                }
                if (isSomeNum.TeenChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new RuConvertTeensToInt());
                }
                if (isSomeNum.TenChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new RuConvertTensToInt());
                }
                if (isSomeNum.HundredChecker(numberList[i]))
                {
                    doConvert.SetConvertToIntStrategy(new RuConvertHundredToInt());
                }
                if (isSomeNum.ThousendChecker(numberList[i]))
                {
                    isMultiplication = true;
                    doConvert.SetConvertToIntStrategy(new RuConvertThousendToInt());
                }
                if (isMultiplication)
                {
                    finalNumber *= doConvert.DoConvertToInt(numberList[i]);
                }
                else
                {
                    finalNumber += doConvert.DoConvertToInt(numberList[i]);
                }
            }
            return finalNumber;
        }
    }
}

