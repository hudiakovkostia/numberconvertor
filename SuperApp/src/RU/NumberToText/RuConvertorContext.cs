﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class RuConvertorContext
    {
        private IConvertNumber _strategy;
        public RuConvertorContext()
        { 
        }
        public RuConvertorContext(IConvertNumber strategy)
        {
            this._strategy = strategy;
        }
        public void SetConvertStrategy(IConvertNumber strategy)
        {
            this._strategy = strategy;
        }
        public string DoConvertToText(int num)
        {
            if (_strategy == null)
                return "";
            return this._strategy.NumberToString(num);
        }
    }
}
