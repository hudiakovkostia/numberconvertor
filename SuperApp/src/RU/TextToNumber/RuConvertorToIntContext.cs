﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class RuConvertorToIntContext
    {
        private IStringToNumber _strategy;
        public RuConvertorToIntContext()
        { 
        }
        public RuConvertorToIntContext(IStringToNumber strategy)
        {
            this._strategy = strategy;
        }
        public void SetConvertToIntStrategy(IStringToNumber strategy)
        {
            this._strategy = strategy;
        }
        public int DoConvertToInt(string textNum)
        {
            if (_strategy == null)
                return 0;
            return this._strategy.StringToNumber(textNum);
        }
    }
}
