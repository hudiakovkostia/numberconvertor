﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class RuDicrionaryContainer
    {
        public Dictionary<int, string> firsts = new Dictionary<int, string>()
        {
            {1, "один"},
            {2, "два"},
            {3, "три"},
            {4, "четыре"},
            {5, "пять"},
            {6, "шесть"},
            {7, "семь"},
            {8, "восемь"},
            {9, "деветь"}
        };
        public Dictionary<int, string> teens = new Dictionary<int, string>()
        {
            {0, "десять"},
            {1, "одиннадцать"},
            {2, "двенадцать"},
            {3, "тринадцать"},
            {4, "четырнадцать"},
            {5, "пятнадцать"},
            {6, "шестнадцать"},
            {7, "семнадцать"},
            {8, "восемнадцать"},
            {9, "девятнадцать" }
        };
        public Dictionary<int, string> tens = new Dictionary<int, string>()
        {
            {2, "двадцать"},
            {3, "тридцать"},
            {4, "сорок"},
            {5, "пятьдесят"},
            {6, "шестьдесят"},
            {7, "семьдесят"},
            {8, "восемьдесят"},
            {9, "девяносто" }
        };
        public Dictionary<int, string> hundreds = new Dictionary<int, string>()
        {
            {1, "сто"},
            {2, "двести"},
            {3, "триста"},
            {4, "четыреста"},
            {5, "пятьсот"},
            {6, "шестьсот"},
            {7, "семьсот"},
            {8, "восемьсот"},
            {9, "девятьсот" }
        };
        public Dictionary<int, string> thousands = new Dictionary<int, string>()
        {
            {1, "тысяча"},
            {2, "тысячи"},
            {3, "тысяч"}
        };
        public Dictionary<int, string> addFirsts = new Dictionary<int, string>()
        {
            {1, "одна"},
            {2, "две"}
        };
    }
}
