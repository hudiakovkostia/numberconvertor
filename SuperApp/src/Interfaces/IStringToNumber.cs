﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    interface IStringToNumber
    {
        int StringToNumber(string textNumber);
    }
}
