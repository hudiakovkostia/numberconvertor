﻿using System;
using System.Windows.Forms;

namespace SuperApp
{
    public partial class Form1 : Form
    {
        LanguageChooseContext languageChoose = new LanguageChooseContext();
        public Form1()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string text = languageChoose.DoConvertNumberToText(Convert.ToInt32(textBox1.Text));
            label1.Text = text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int text = languageChoose.DoConvertTextToNumber(textBox2.Text);
            label2.Text = text.ToString();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            languageChoose.SetConvertToIntStrategy(new RuNumberConvertor());
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                languageChoose.SetConvertToIntStrategy(new RuNumberConvertor());
            }
            if (radioButton2.Checked)
            {
                languageChoose.SetConvertToIntStrategy(new EngNumberConvertor());
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            languageChoose.SetConvertToIntStrategy(new EngNumberConvertor());
        }
    }
}

