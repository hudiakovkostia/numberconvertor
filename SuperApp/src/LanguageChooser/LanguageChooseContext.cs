﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperApp
{
    class LanguageChooseContext
    {
        private ILanguageChoose _strategy;
        public LanguageChooseContext()
        { 
        }
        public LanguageChooseContext(ILanguageChoose strategy)
        {
            this._strategy = strategy;
        }
        public void SetConvertToIntStrategy(ILanguageChoose strategy)
        {
            this._strategy = strategy;
        }
        public int DoConvertTextToNumber(string textNum)
        {
            if (_strategy == null)
                return 0;
            return this._strategy.ConvertTextToNumber(textNum);
        }
        public string DoConvertNumberToText(int number)
        {
            if (_strategy == null)
                return "";
            return this._strategy.ConvertNumberToText(number);
        }
    }
}
